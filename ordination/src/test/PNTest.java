package test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import model.PN;

public class PNTest {

    private PN pn2, pn4;
    private LocalDate date1, date2, start1, slut1, giv1, giv2, giv3;

    @Before
    public void setUp() {
        date1 = LocalDate.of(2018, 1, 15);
        date2 = LocalDate.of(2018, 2, 10);
        start1 = LocalDate.of(2018, 1, 1);
        slut1 = LocalDate.of(2018, 1, 20);

        giv1 = LocalDate.of(2018, 1, 2);
        giv2 = LocalDate.of(2018, 1, 3);
        giv3 = LocalDate.of(2018, 1, 5);

        pn2 = new PN(start1, slut1, 2);
        pn4 = new PN(start1, slut1, 4);
    }

    // --------------- GIV DOSIS -----------------------
    @Test
    public void testGivDosis1() {
        assertEquals(true, pn2.givDosis(date1));
        assertEquals(1, pn2.getAntalGangeGivet());
    }

    @Test
    public void testGivDosis2() {
        assertEquals(false, pn2.givDosis(date2));
        assertEquals(0, pn2.getAntalGangeGivet());
    }

    // --------------- DOEGN DOSIS -----------------------
    @Test
    public void testDoegnDosis1() {
        pn2.givDosis(giv1);
        pn2.givDosis(giv2);

        assertEquals(2, pn2.doegnDosis(), 0.01);
    }

    @Test
    public void testDoegnDosis2() {
        pn4.givDosis(giv1);
        pn4.givDosis(giv2);

        assertEquals(4, pn4.doegnDosis(), 0.001);
    }

    @Test
    public void testDoegnDosis3() {
        pn2.givDosis(giv1);
        pn2.givDosis(giv3);

        assertEquals(1, pn2.doegnDosis(), 0.001);
    }

    @Test
    public void testDoegnDosis4() {
        pn4.givDosis(giv1);
        pn4.givDosis(giv3);

        assertEquals(2, pn4.doegnDosis(), 0.001);
    }

    // --------------- SAMLET DOSIS -----------------------

    @Test
    public void testSamletDosis1() {
        pn2.givDosis(giv1);
        pn2.givDosis(giv2);

        assertEquals(4, pn2.samletDosis(), 0.001);
    }

    @Test
    public void testSamletDosis2() {
        pn2.givDosis(giv1);
        pn2.givDosis(giv2);
        pn2.givDosis(giv3);

        assertEquals(6, pn2.samletDosis(), 0.001);
    }

    @Test
    public void testSamletDosis3() {
        PN pn0 = new PN(start1, slut1, 0);
        pn0.givDosis(giv1);
        pn0.givDosis(giv3);

        assertEquals(0, pn0.samletDosis(), 0.001);
    }

}
