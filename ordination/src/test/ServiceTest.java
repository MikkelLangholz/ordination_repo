package test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import model.DagligFast;
import model.DagligSkaev;
import model.Dosis;
import model.Laegemiddel;
import model.PN;
import model.Patient;
import service.Service;
import storage.Storage;

public class ServiceTest {
	private Patient p1, p2;
	private Laegemiddel l1, l2;
	private Storage sr;
	private Service sv;

	@Before
	public void setUp() {
		sr = new Storage();
		sv = new Service(sr);

		p1 = sv.opretPatient("121256-0512", "Jane Jensen", 63.4);
		p2 = sv.opretPatient("011064-1522", "Ulla Nielsen", 59.9);

		l1 = sv.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		l2 = sv.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");

	}

	@Test(expected = IllegalArgumentException.class)
	public void testOpretPNOrdination1() {
		// Arrange
		PN pn = sv.opretPNOrdination(LocalDate.of(2018, 1, 1), LocalDate.of(2017, 1, 1), p1, l1, 1);
	}

	@Test
	public void testOpretPNOrdination2() {
		// Arrange
		PN pn = sv.opretPNOrdination(LocalDate.of(2018, 1, 1), LocalDate.of(2018, 1, 5), p1, l1, 1);
		// Act
		LocalDate startDen = pn.getStartDen();
		LocalDate slutDen = pn.getSlutDen();
		double antal = pn.getAntalEnheder();
		// Assert
		assertEquals(LocalDate.of(2018, 1, 1), startDen);
		assertEquals(LocalDate.of(2018, 1, 5), slutDen);
		assertEquals(1, antal, 0.001);
		assertEquals(true, p1.getOrdinationer().contains(pn));
		assertEquals(l1, pn.getLaegemiddel());
	}

	@Test
	public void testOpretPNOrdination3() {
		// Arrange
		PN pn = sv.opretPNOrdination(LocalDate.of(2018, 1, 1), LocalDate.of(2019, 1, 1), p2, l2, 5);
		// Act
		LocalDate startDen = pn.getStartDen();
		LocalDate slutDen = pn.getSlutDen();
		double antal = pn.getAntalEnheder();
		// Assert
		assertEquals(LocalDate.of(2018, 1, 1), startDen);
		assertEquals(LocalDate.of(2019, 1, 1), slutDen);
		assertEquals(5, antal, 0.001);
		assertEquals(true, p2.getOrdinationer().contains(pn));
		assertEquals(l2, pn.getLaegemiddel());
	}

	@Test
	public void testOpretDagligFastOrdination1() {
		// Arrange
		DagligFast df = sv.opretDagligFastOrdination(LocalDate.of(2018, 1, 1), LocalDate.of(2018, 1, 3), p1, l1, 2, 2,
				2, 0);
		// Act
		LocalDate startDen = df.getStartDen();
		LocalDate slutDen = df.getSlutDen();
		Laegemiddel lm = df.getLaegemiddel();
		double d1 = df.getDoser()[0].getAntal();
		double d2 = df.getDoser()[1].getAntal();
		double d3 = df.getDoser()[2].getAntal();
		Dosis d4 = df.getDoser()[3];
		// Assert
		assertEquals(LocalDate.of(2018, 1, 1), startDen);
		assertEquals(LocalDate.of(2018, 1, 3), slutDen);
		assertEquals(true, p1.getOrdinationer().contains(df));
		assertEquals(lm, df.getLaegemiddel());
		assertEquals(2, d1, 0.001);
		assertEquals(2, d2, 0.001);
		assertEquals(2, d3, 0.001);
		assertEquals(null, d4);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOpretDagligFastOrdination2() {
		DagligFast df = sv.opretDagligFastOrdination(LocalDate.of(2018, 1, 3), LocalDate.of(2018, 1, 1), p1, l1, 2, 2,
				2, 2);
	}

	@Test
	public void testOpretDagligFastOrdination3() {
		// Arrange
		DagligFast df = sv.opretDagligFastOrdination(LocalDate.of(2018, 1, 1), LocalDate.of(2018, 1, 3), p1, l1, 2, 2,
				2, 0);
		// Act
		LocalDate startDen = df.getStartDen();
		LocalDate slutDen = df.getSlutDen();
		Laegemiddel lm = df.getLaegemiddel();
		double d1 = df.getDoser()[0].getAntal();
		double d2 = df.getDoser()[1].getAntal();
		double d3 = df.getDoser()[2].getAntal();
		Dosis d4 = df.getDoser()[3];
		// Assert
		assertEquals(LocalDate.of(2018, 1, 1), startDen);
		assertEquals(LocalDate.of(2018, 1, 3), slutDen);
		assertEquals(true, p1.getOrdinationer().contains(df));
		assertEquals(lm, df.getLaegemiddel());
		assertEquals(2, d1, 0.001);
		assertEquals(2, d2, 0.001);
		assertEquals(2, d3, 0.001);
		assertEquals(null, d4);

	}

	@Test(expected = IllegalArgumentException.class)
	public void testOpretDagligSkaevOrdination1() {
		// Arrange
		LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40), LocalTime.of(16, 0), LocalTime.of(18, 45) };
		double[] an = { 0.5, 1, 2.5, 3 };
		DagligSkaev ds = sv.opretDagligSkaevOrdination(LocalDate.of(2018, 1, 1), LocalDate.of(2017, 1, 1), p1, l1, kl,
				an);
	}

	@Test
	public void testOpretDagligSkaevOrdination2() {
		// Arrange
		LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40), LocalTime.of(16, 0), LocalTime.of(18, 45) };
		double[] an = { 0.5, 1, 2.5, 3 };
		DagligSkaev ds = sv.opretDagligSkaevOrdination(LocalDate.of(2018, 1, 1), LocalDate.of(2018, 1, 5), p1, l1, kl,
				an);
		// Act
		LocalDate startDen = ds.getStartDen();
		LocalDate slutDen = ds.getSlutDen();
		LocalTime kl0 = ds.getDoser().get(0).getTid();
		LocalTime kl1 = ds.getDoser().get(1).getTid();
		LocalTime kl2 = ds.getDoser().get(2).getTid();
		LocalTime kl3 = ds.getDoser().get(3).getTid();
		double an0 = ds.getDoser().get(0).getAntal();
		double an1 = ds.getDoser().get(1).getAntal();
		double an2 = ds.getDoser().get(2).getAntal();
		double an3 = ds.getDoser().get(3).getAntal();
		// Assert
		assertEquals(LocalDate.of(2018, 1, 1), startDen);
		assertEquals(LocalDate.of(2018, 1, 5), slutDen);
		assertEquals(LocalTime.of(12, 00), kl0);
		assertEquals(LocalTime.of(12, 40), kl1);
		assertEquals(LocalTime.of(16, 00), kl2);
		assertEquals(LocalTime.of(18, 45), kl3);
		assertEquals(0.5, an0, 0.001);
		assertEquals(1, an1, 0.001);
		assertEquals(2.5, an2, 0.001);
		assertEquals(3, an3, 0.001);
		assertEquals(true, p1.getOrdinationer().contains(ds));
		assertEquals(l1, ds.getLaegemiddel());
	}

	@Test
	public void testOpretDagligSkaevOrdination3() {
		// Arrange
		LocalTime[] kl = { LocalTime.of(8, 0), LocalTime.of(12, 30), LocalTime.of(18, 0) };
		double[] an = { 2, 1, 3 };
		DagligSkaev ds = sv.opretDagligSkaevOrdination(LocalDate.of(2018, 1, 1), LocalDate.of(2019, 1, 1), p2, l2, kl,
				an);
		// Act
		LocalDate startDen = ds.getStartDen();
		LocalDate slutDen = ds.getSlutDen();
		LocalTime kl0 = ds.getDoser().get(0).getTid();
		LocalTime kl1 = ds.getDoser().get(1).getTid();
		LocalTime kl2 = ds.getDoser().get(2).getTid();
		double an0 = ds.getDoser().get(0).getAntal();
		double an1 = ds.getDoser().get(1).getAntal();
		double an2 = ds.getDoser().get(2).getAntal();
		// Assert
		assertEquals(LocalDate.of(2018, 1, 1), startDen);
		assertEquals(LocalDate.of(2019, 1, 1), slutDen);
		assertEquals(LocalTime.of(8, 00), kl0);
		assertEquals(LocalTime.of(12, 30), kl1);
		assertEquals(LocalTime.of(18, 00), kl2);
		assertEquals(2, an0, 0.001);
		assertEquals(1, an1, 0.001);
		assertEquals(3, an2, 0.001);
		assertEquals(true, p2.getOrdinationer().contains(ds));
		assertEquals(l2, ds.getLaegemiddel());
	}

}
