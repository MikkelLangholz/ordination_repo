package test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import model.Laegemiddel;
import model.PN;
import model.Patient;
import service.Service;
import storage.Storage;

public class ServiceTest2 {
    private Service service;
    private LocalDate startDen, slutDen;
    private PN pn;
    private Patient p24, p25, p120, p121;
    private Laegemiddel acetylsalicylsyre, paracetamol, fucidin, methotrexat;

    @Before
    public void setUp() {
        Storage storage = new Storage();
        service = new Service(storage);
        startDen = LocalDate.of(2018, 2, 15);
        slutDen = LocalDate.of(2018, 2, 28);

        pn = new PN(startDen, slutDen, 2);

        p24 = service.opretPatient("1111111111", "p24", 24);
        p25 = service.opretPatient("2222222222", "p25", 25);
        p120 = service.opretPatient("3333333333", "p120", 120);
        p121 = service.opretPatient("4444444444", "p121", 121);

        acetylsalicylsyre = service.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
        paracetamol = service.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
        fucidin = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
        methotrexat = service.opretLaegemiddel("Methotrexat", 0.01, 0.015, 0.02, "Styk");

        service.opretPNOrdination(startDen, slutDen, p24, paracetamol, 2);
        service.opretPNOrdination(startDen, slutDen, p25, paracetamol, 2);
        service.opretPNOrdination(startDen, slutDen, p25, acetylsalicylsyre, 2);
        service.opretDagligFastOrdination(startDen, slutDen, p120, paracetamol, 1, 1, 0, 1);
    }

    // --------------- ORDINATION ANVENDT -------------------------
    @Test
    public void testOrdinationPNAnvendt1() {
        service.ordinationPNAnvendt(pn, LocalDate.of(2018, 2, 15));
        assertEquals(1, pn.getAntalGangeGivet());

    }

    @Test
    public void testOrdinationPNAnvendt2() {
        service.ordinationPNAnvendt(pn, LocalDate.of(2018, 2, 20));
        assertEquals(1, pn.getAntalGangeGivet());

    }

    @Test(expected = IllegalArgumentException.class)
    public void testOrdinationPNAnvendt3() {
        service.ordinationPNAnvendt(pn, LocalDate.of(2018, 1, 20));
    }

    @Test
    public void testOrdinationPNAnvendt4() {
        service.ordinationPNAnvendt(pn, LocalDate.of(2018, 2, 28));
        assertEquals(1, pn.getAntalGangeGivet());

    }

    // ------------------- ANBEFALET DOSIS ---------------------------
    @Test
    public void testAnbefaletDosisPrDoegn1() {
        assertEquals(2.4, service.anbefaletDosisPrDoegn(p24, acetylsalicylsyre), 0.01);
    }

    @Test
    public void testAnbefaletDosisPrDoegn2() {
        assertEquals(24, service.anbefaletDosisPrDoegn(p24, paracetamol), 0.01);
    }

    @Test
    public void testAnbefaletDosisPrDoegn3() {
        assertEquals(0.6, service.anbefaletDosisPrDoegn(p24, fucidin), 0.01);
    }

    @Test
    public void testAnbefaletDosisPrDoegn4() {
        assertEquals(0.24, service.anbefaletDosisPrDoegn(p24, methotrexat), 0.01);
    }

    @Test
    public void testAnbefaletDosisPrDoegn5() {
        assertEquals(3.75, service.anbefaletDosisPrDoegn(p25, acetylsalicylsyre), 0.01);
    }

    @Test
    public void testAnbefaletDosisPrDoegn6() {
        assertEquals(37.5, service.anbefaletDosisPrDoegn(p25, paracetamol), 0.01);
    }

    @Test
    public void testAnbefaletDosisPrDoegn7() {
        assertEquals(0.625, service.anbefaletDosisPrDoegn(p25, fucidin), 0.01);
    }

    @Test
    public void testAnbefaletDosisPrDoegn8() {
        assertEquals(0.375, service.anbefaletDosisPrDoegn(p25, methotrexat), 0.01);
    }

    @Test
    public void testAnbefaletDosisPrDoegn9() {
        assertEquals(18, service.anbefaletDosisPrDoegn(p120, acetylsalicylsyre), 0.01);
    }

    @Test
    public void testAnbefaletDosisPrDoegn10() {
        assertEquals(180, service.anbefaletDosisPrDoegn(p120, paracetamol), 0.01);
    }

    @Test
    public void testAnbefaletDosisPrDoegn11() {
        assertEquals(3, service.anbefaletDosisPrDoegn(p120, fucidin), 0.01);
    }

    @Test
    public void testAnbefaletDosisPrDoegn12() {
        assertEquals(1.8, service.anbefaletDosisPrDoegn(p120, methotrexat), 0.01);
    }

    @Test
    public void testAnbefaletDosisPrDoegn13() {
        assertEquals(19.36, service.anbefaletDosisPrDoegn(p121, acetylsalicylsyre), 0.01);
    }

    @Test
    public void testAnbefaletDosisPrDoegn14() {
        assertEquals(242, service.anbefaletDosisPrDoegn(p121, paracetamol), 0.01);
    }

    @Test
    public void testAnbefaletDosisPrDoegn15() {
        assertEquals(3.025, service.anbefaletDosisPrDoegn(p121, fucidin), 0.01);
    }

    @Test
    public void testAnbefaletDosisPrDoegn16() {
        assertEquals(2.42, service.anbefaletDosisPrDoegn(p121, methotrexat), 0.01);
    }

    // ----------ORDINATIONER PER VÆGT PER LÆGEMIDDEL ---------------
    @Test
    public void testAntalOrdinationerPrVægtPrLægemiddel1() {
        int actual = service.antalOrdinationerPrVægtPrLægemiddel(20, 130, paracetamol);
        assertEquals(3, actual);
    }

    @Test
    public void testAntalOrdinationerPrVægtPrLægemiddel2() {
        int actual = service.antalOrdinationerPrVægtPrLægemiddel(0, 100, paracetamol);
        assertEquals(2, actual);
    }

    @Test
    public void testAntalOrdinationerPrVægtPrLægemiddel3() {
        int actual = service.antalOrdinationerPrVægtPrLægemiddel(25, 130, acetylsalicylsyre);
        assertEquals(1, actual);
    }

    @Test
    public void testAntalOrdinationerPrVægtPrLægemiddel4() {
        int actual = service.antalOrdinationerPrVægtPrLægemiddel(0, 20, acetylsalicylsyre);
        assertEquals(0, actual);
    }

}
