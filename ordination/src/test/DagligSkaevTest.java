package test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

import model.DagligSkaev;
import model.Dosis;

public class DagligSkaevTest {

	@Test
	public void testOpretDosis1() {
		// Arrange
		DagligSkaev ds = new DagligSkaev(LocalDate.of(2015, 1, 23), LocalDate.of(2015, 1, 24));
		ds.opretDosis(LocalTime.of(17, 00), 3);
		Dosis dosis = ds.getDoser().get(0);
		// Act
		LocalTime tid = dosis.getTid();
		double antal = dosis.getAntal();
		int lenght = ds.getDoser().size();
		// Assert
		assertEquals(LocalTime.of(17, 00), tid);
		assertEquals(3, antal, 0.001);
		assertEquals(1, lenght);
	}

	@Test
	public void testOpretDosis2() {
		// Arrange
		DagligSkaev ds = new DagligSkaev(LocalDate.of(2015, 1, 23), LocalDate.of(2015, 1, 24));
		ds.opretDosis(LocalTime.of(19, 00), 2);
		Dosis dosis = ds.getDoser().get(0);
		// Act
		LocalTime tid = dosis.getTid();
		double antal = dosis.getAntal();
		int lenght = ds.getDoser().size();
		// Assert
		assertEquals(LocalTime.of(19, 00), tid);
		assertEquals(2, antal, 0.001);
		assertEquals(1, lenght);
	}

	@Test
	public void testSamletDosis1() {
		// Arrange
		DagligSkaev ds = new DagligSkaev(LocalDate.of(2018, 1, 1), LocalDate.of(2018, 1, 1));
		ds.opretDosis(LocalTime.of(17, 00), 3);
		// Act
		double sd = ds.samletDosis();
		// Assert
		assertEquals(3, sd, 0.001);
	}

	@Test
	public void testSamletDosis2() {
		// Arrange
		DagligSkaev ds = new DagligSkaev(LocalDate.of(2018, 1, 1), LocalDate.of(2018, 1, 5));
		ds.opretDosis(LocalTime.of(17, 00), 3);
		// Act
		double sd = ds.samletDosis();
		// Assert
		assertEquals(15, sd, 0.001);
	}

	@Test
	public void testDoegnDosis1() {
		// Arrange
		DagligSkaev ds = new DagligSkaev(LocalDate.of(2018, 1, 1), LocalDate.of(2018, 1, 1));
		ds.opretDosis(LocalTime.of(17, 00), 3);
		ds.opretDosis(LocalTime.of(17, 00), 4);
		// Act
		double dd = ds.doegnDosis();
		// Assert
		assertEquals(7, dd, 0.001);
	}

	@Test
	public void testDoegnDosis2() {
		// Arrange
		DagligSkaev ds = new DagligSkaev(LocalDate.of(2018, 1, 1), LocalDate.of(2018, 1, 1));
		ds.opretDosis(LocalTime.of(17, 00), 5);
		ds.opretDosis(LocalTime.of(17, 00), 2);
		ds.opretDosis(LocalTime.of(17, 00), 3);
		// Act
		double dd = ds.doegnDosis();
		// Assert
		assertEquals(10, dd, 0.001);
	}

}
