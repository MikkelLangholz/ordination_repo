package test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

import model.DagligFast;
import model.Dosis;
import model.Laegemiddel;
import model.Patient;

public class DagligFastTest {

	private DagligFast df;
	private Patient p1, p2, p3, p4, p5;
	private Laegemiddel lm1, lm2, lm3, lm4;

	@Test
	public void testSamletDosis() {
		// Arrange
		DagligFast df = new DagligFast(LocalDate.of(2018, 1, 1), LocalDate.of(2018, 1, 1));
		df.opretDosis(0, 1, 1, 1);
		// Act
		double sd = df.samletDosis();
		// Assert
		assertEquals(3, sd, 0.001);
	}

	@Test
	public void testSamletDosis1() {
		// Arrange
		DagligFast df = new DagligFast(LocalDate.of(2018, 1, 1), LocalDate.of(2018, 1, 5));
		df.opretDosis(0, 1, 1, 1);
		// Act
		double sd = df.samletDosis();
		// Assert
		assertEquals(15, sd, 0.001);
	}

	@Test
	public void testSamletDosis2() {
		// Arrange
		DagligFast df = new DagligFast(LocalDate.of(2018, 1, 1), LocalDate.of(2018, 1, 1));
		df.opretDosis(0, 0, 0, 0);
		// Act
		double sd = df.samletDosis();
		// Assert
		assertEquals(0, sd, 0.001);
	}

	@Test
	public void testDoegnDosis() {
		// Arrange
		DagligFast df = new DagligFast(LocalDate.of(2018, 1, 1), LocalDate.of(2018, 1, 1));
		df.opretDosis(0, 1, 1, 1);
		// Act
		double dd = df.doegnDosis();
		// Assert
		assertEquals(3, dd, 0.001);
	}

	@Test
	public void testDoegnDosis1() {
		// Arrange
		DagligFast df = new DagligFast(LocalDate.of(2018, 1, 1), LocalDate.of(2018, 1, 1));
		df.opretDosis(1, 0, 1, 1);
		// Act
		double dd = df.doegnDosis();
		// Assert
		assertEquals(3, dd, 0.001);
	}

	@Test
	public void testDoegnDosis2() {
		// Arrange
		DagligFast df = new DagligFast(LocalDate.of(2018, 1, 1), LocalDate.of(2018, 1, 1));
		df.opretDosis(1, 1, 0, 1);
		// Act
		double dd = df.doegnDosis();
		// Assert
		assertEquals(3, dd, 0.001);
	}

	@Test
	public void testDoegnDosis3() {
		// Arrange
		DagligFast df = new DagligFast(LocalDate.of(2018, 1, 1), LocalDate.of(2018, 1, 1));
		df.opretDosis(1, 1, 1, 0);
		// Act
		double dd = df.doegnDosis();
		// Assert
		assertEquals(3, dd, 0.001);
	}

	@Test
	public void testDoegnDosis4() {
		// Arrange
		DagligFast df = new DagligFast(LocalDate.of(2018, 1, 1), LocalDate.of(2018, 1, 1));
		df.opretDosis(0, 0, 0, 0);
		// Act
		double dd = df.doegnDosis();
		// Assert
		assertEquals(0, dd, 0.001);
	}

	@Test
	public void testDoegnDosis5() {
		// Arrange
		DagligFast df = new DagligFast(LocalDate.of(2018, 1, 1), LocalDate.of(2018, 1, 1));
		df.opretDosis(2, 3, 0, 3);
		// Act
		double dd = df.doegnDosis();
		// Assert
		assertEquals(8, dd, 0.001);
	}

	@Test
	public void testGetType() {
		// Arrange
		DagligFast df = new DagligFast(LocalDate.of(2018, 1, 1), LocalDate.of(2018, 1, 1));
		// Act
		String type = df.getType();
		// Assert
		assertEquals("DagligFast", type);
	}

	@Test
	public void testGetDoser() {
		// Arrange
		DagligFast df = new DagligFast(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02));
		df.opretDosis(0, 3, 2, 4);
		// Act
		Dosis[] doser = df.getDoser();
		// Assert
		assertEquals(null, doser[0]);
		assertEquals(3, doser[1].getAntal(), 0.001);
		assertEquals(2, doser[2].getAntal(), 0.001);
		assertEquals(4, doser[3].getAntal(), 0.001);
	}

	@Test
	public void testOpretDosis() {
		// Arrange
		DagligFast df = new DagligFast(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02));
		df.opretDosis(0, 1, 1, 1);
		// Act
		Dosis d1 = df.getDoser()[0];
		double d2 = df.getDoser()[1].getAntal();
		double d3 = df.getDoser()[2].getAntal();
		double d4 = df.getDoser()[3].getAntal();
		LocalDate start = df.getStartDen();
		LocalDate slut = df.getSlutDen();
		// Assert
		assertEquals(null, d1);
		assertEquals(1, d2, 0.001);
		assertEquals(1, d3, 0.001);
		assertEquals(1, d4, 0.001);
		assertEquals(LocalDate.of(2018, 01, 01), start);
		assertEquals(LocalDate.of(2018, 01, 02), slut);
	}

	@Test
	public void testOpretDosis1() {
		// Arrange
		DagligFast df = new DagligFast(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02));
		df.opretDosis(1, 0, 1, 1);
		// Act
		double d1 = df.getDoser()[0].getAntal();
		Dosis d2 = df.getDoser()[1];
		double d3 = df.getDoser()[2].getAntal();
		double d4 = df.getDoser()[3].getAntal();
		LocalDate start = df.getStartDen();
		LocalDate slut = df.getSlutDen();
		// Assert
		assertEquals(1, d1, 0.001);
		assertEquals(null, d2);
		assertEquals(1, d3, 0.001);
		assertEquals(1, d4, 0.001);
		assertEquals(LocalDate.of(2018, 01, 01), start);
		assertEquals(LocalDate.of(2018, 01, 02), slut);
	}

	@Test
	public void testOpretDosis2() {
		// Arrange
		DagligFast df = new DagligFast(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02));
		df.opretDosis(1, 1, 0, 1);
		// Act
		double d1 = df.getDoser()[0].getAntal();
		double d2 = df.getDoser()[1].getAntal();
		Dosis d3 = df.getDoser()[2];
		double d4 = df.getDoser()[3].getAntal();
		LocalDate start = df.getStartDen();
		LocalDate slut = df.getSlutDen();
		// Assert
		assertEquals(1, d1, 0.001);
		assertEquals(1, d2, 0.001);
		assertEquals(null, d3);
		assertEquals(1, d4, 0.001);
		assertEquals(LocalDate.of(2018, 01, 01), start);
		assertEquals(LocalDate.of(2018, 01, 02), slut);
	}

	@Test
	public void testOpretDosis3() {
		// Arrange
		DagligFast df = new DagligFast(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02));
		df.opretDosis(1, 1, 1, 0);
		// Act
		double d1 = df.getDoser()[0].getAntal();
		double d2 = df.getDoser()[1].getAntal();
		double d3 = df.getDoser()[2].getAntal();
		Dosis d4 = df.getDoser()[3];
		LocalDate start = df.getStartDen();
		LocalDate slut = df.getSlutDen();
		// Assert
		assertEquals(1, d1, 0.001);
		assertEquals(1, d2, 0.001);
		assertEquals(1, d3, 0.001);
		assertEquals(null, d4);
		assertEquals(LocalDate.of(2018, 01, 01), start);
		assertEquals(LocalDate.of(2018, 01, 02), slut);
	}

	@Test
	public void testOpretDosis4() {
		// Arrange
		DagligFast df = new DagligFast(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02));
		df.opretDosis(0, 0, 0, 0);
		// Act
		Dosis d1 = df.getDoser()[0];
		Dosis d2 = df.getDoser()[1];
		Dosis d3 = df.getDoser()[2];
		Dosis d4 = df.getDoser()[3];
		LocalDate start = df.getStartDen();
		LocalDate slut = df.getSlutDen();
		// Assert
		assertEquals(null, d1);
		assertEquals(null, d2);
		assertEquals(null, d3);
		assertEquals(null, d4);
		assertEquals(LocalDate.of(2018, 01, 01), start);
		assertEquals(LocalDate.of(2018, 01, 02), slut);
	}

}
