package model;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class PN extends Ordination {
    private final double antalEnheder;
    private List<LocalDate> datoForGivetDosis;

    public PN(LocalDate startDen, LocalDate slutDen, double antalEnheder) {
        super(startDen, slutDen);
        this.antalEnheder = antalEnheder;
        this.datoForGivetDosis = new ArrayList<>();
    }

    /**
     * Registrerer at der er givet en dosis paa dagen givesDen. Returnerer true,
     * hvis givesDen er inden for ordinationens gyldighedsperiode, og datoen huskes.
     * Returnerer false ellers og datoen givesDen ignoreres.
     */
    public boolean givDosis(LocalDate givesDen) {
        // Trækker en dato fra start og ligger en til slut, så isAfter og isBefore kan
        // bruges. De 2 metoder inkluderer ikke datoen som testes
        LocalDate tempStart = getStartDen().minusDays(1);
        LocalDate tempSlut = getSlutDen().plusDays(1);
        Boolean givDosis = false;
        if (givesDen.isAfter(tempStart) && givesDen.isBefore(tempSlut)) {
            this.datoForGivetDosis.add(givesDen);
            givDosis = true;
        }
        return givDosis;
    }

    /**
     * Returnerer antal gange ordinationen er anvendt.
     */
    public int getAntalGangeGivet() {
        return this.datoForGivetDosis.size();
    }

    public double getAntalEnheder() {
        return antalEnheder;
    }

    @Override
    public double samletDosis() {
        return getAntalGangeGivet() * antalEnheder;
    }

    /**
     * Returnerer den gennemsnitlige dosis givet pr dag fra første dag til sidste
     * dag medicinen er taget
     */
    @Override
    public double doegnDosis() {
        if (datoForGivetDosis.size() > 0) {
            LocalDate foersteGivning = this.datoForGivetDosis.get(0);
            LocalDate sidsteGivning = this.datoForGivetDosis.get(getAntalGangeGivet() - 1);

            for (LocalDate ld : this.datoForGivetDosis) {
                if (ld.isBefore(foersteGivning)) {
                    foersteGivning = ld;
                }
                if (ld.isAfter(sidsteGivning)) {
                    sidsteGivning = ld;
                }
            }
            long daysBetween = ChronoUnit.DAYS.between(foersteGivning, sidsteGivning);
            return samletDosis() / (daysBetween + 1);
        } else {
            return 0;
        }
    }

    @Override
    public String getType() {
        return "PN";
    }

}
