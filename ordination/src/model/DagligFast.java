package model;

import java.time.LocalDate;
import java.time.LocalTime;

public class DagligFast extends Ordination {

    Dosis[] doser = new Dosis[4];

    public DagligFast(LocalDate startDen, LocalDate slutDen) {
        super(startDen, slutDen);
    }

    public Dosis[] getDoser() {
        return doser;
    }

    public void opretDosis(double morgenAntal, double middagsAntal, double aftensAntal, double natAntal) {
        if (morgenAntal > 0) {
            doser[0] = new Dosis(LocalTime.of(6, 0), morgenAntal);
        }
        if (middagsAntal > 0) {
            doser[1] = new Dosis(LocalTime.of(12, 0), middagsAntal);
        }
        if (aftensAntal > 0) {
            doser[2] = new Dosis(LocalTime.of(18, 0), aftensAntal);
        }
        if (natAntal > 0) {
            doser[3] = new Dosis(LocalTime.of(0, 0), natAntal);
        }
    }

    @Override
    public double samletDosis() {
        return doegnDosis() * antalDage();
    }

    @Override
    public double doegnDosis() {
        double doegnDosis = 0;
        for (int i = 0; i < 4; i++) {
            if (doser[i] != null) {
                doegnDosis += doser[i].getAntal();
            }
        }
        return doegnDosis;
    }

    @Override
    public String getType() {
        return "DagligFast";
    }

}