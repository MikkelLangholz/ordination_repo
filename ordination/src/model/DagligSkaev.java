package model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class DagligSkaev extends Ordination {
	List<Dosis> doser = new ArrayList<>();

	public DagligSkaev(LocalDate startDen, LocalDate slutDen) {
		super(startDen, slutDen);

	}

	public ArrayList<Dosis> getDoser() {
		return new ArrayList<>(doser);
	}

	/** Pre: antal >= 0. */
	public void opretDosis(LocalTime tid, double antal) {
		assert antal >= 0;
		Dosis dosis = new Dosis(tid, antal);
		doser.add(dosis);
	}

	public void removeDosis(Dosis dosis) {
		doser.remove(dosis);
	}

	@Override
	public double samletDosis() {
		return doegnDosis() * antalDage();
	}

	@Override
	public double doegnDosis() {
		double doegnDosis = 0;
		for (Dosis d : doser) {
			doegnDosis += d.getAntal();
		}
		return doegnDosis;
	}

	@Override
	public String getType() {
		return "DagligSkaev";
	}

}
